import express, {Request, Response} from 'express';
import {Order} from '../model/Order';
import {CategoriesService} from '../service/CategoriesService';
export class CategoriesController {
  async findCategories(req: Request, res: Response) {
    try {
      const categoriesService = new CategoriesService();
      const categories = await categoriesService.getAllCategories();
      res.status(200).send(categories);
    } catch (err: any) {
      console.log(err);
      res.status(500).send(err.message);
    }
  }
}
