import express, {Request, Response} from 'express';
import {Order, ORDER_SCHEMA} from '../model/Order';
import {OrdersService} from '../service/OrdersService';
import * as Validator from '../utils/Validator';
export class OrdersController {
  async createOrder(req: Request, res: Response) {
    try {
      const order: Order = req.body;
      Validator.validateSchema(order, ORDER_SCHEMA);
      const ordersService = new OrdersService();
      await ordersService.postAnOrder(order);
      res.status(201).send({});
    } catch (err: any) {
      console.log(err);
      res.status(500).send(err.message);
    }
  }

  async viewAllOrders(req: Request, res: Response): Promise<void> {
    try {
      const ordersService = new OrdersService();
      const orders = await ordersService.findAllOrders();
      res.status(200).send(orders);
    } catch (err: any) {
      console.log(err);
      res.status(500).send(err.message);
    }
  }
}
