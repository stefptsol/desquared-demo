import express, {Request, Response} from 'express';
import {CurrencyCode} from '../model/CurrencyCode';
import {Order} from '../model/Order';
import {ProductsService} from '../service/ProductsService';
export class ProductsController {
  async viewAllProducts(req: Request, res: Response): Promise<void> {
    try {
      const categoryId = req.params.categoryId;
      const currencyCode = req.query.currency as CurrencyCode;
      if (currencyCode && !Object.values(CurrencyCode).includes(currencyCode)) {
        throw new Error('Currency code not supported');
      }
      const productsService = new ProductsService();
      const products = await productsService.getProductsByCategory(
        categoryId,
        currencyCode
      );
      res.status(200).send(products);
    } catch (err: any) {
      console.log(err);
      res.status(500).send(err.message);
    }
  }
}
