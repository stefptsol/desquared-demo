import {CurrencyCode} from './CurrencyCode';

export interface Price {
  value: number;
  currency: CurrencyCode;
}
