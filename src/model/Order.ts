import * as Joi from 'joi';
import {OrderItem, ORDER_ITEM_SCHEMA} from './OrderItems';

export interface Order {
  orderItems: OrderItem[];
}

export const ORDER_SCHEMA = Joi.object({
  orderItems: Joi.array().items(ORDER_ITEM_SCHEMA),
}).required();
