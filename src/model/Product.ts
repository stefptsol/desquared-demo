import {Price} from './Price';

export interface Product {
  _id: string;
  categoryId: string;
  price: Price;
  description: string;
  /**
   * The number of products that are in stock
   */
  quantity: number;
}
