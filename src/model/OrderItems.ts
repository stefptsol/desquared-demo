import Joi from 'joi';

export interface OrderItem {
  productId: string;
  quantity: number;
}

export const ORDER_ITEM_SCHEMA = Joi.object({
  productId: Joi.string().required(),
  quantity: Joi.number().required(),
}).required();
