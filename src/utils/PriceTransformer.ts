import {FixerClient} from '../client/FixerClient';
import {CurrencyCode} from '../model/CurrencyCode';
import {Product} from '../model/Product';
import {MoneyRateInterface} from './MoneyRateInterface';

/**
 * Takes a lists of products and transfrom their price from euro to the selected currency
 */
export class PriceTransformer {
  async toCurrency(
    products: Product[],
    currencyCode: CurrencyCode
  ): Promise<Product[]> {
    const moneyRateClient: MoneyRateInterface = new FixerClient();
    const rate = await moneyRateClient.getRateToday(currencyCode);
    for (let product of products) {
      product.price.currency = currencyCode;
      product.price.value = product.price.value * rate;
    }
    return products;
  }
}
