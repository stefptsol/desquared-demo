import {CurrencyCode} from '../model/CurrencyCode';

export interface MoneyRateInterface {
  /**
   * Returns the exchange rate of a currency to euro
   * @param currencyCode 
   */
  getRateToday(currencyCode: CurrencyCode): Promise<number>;
}
