const options = {
  allowUnkown: true,
  stripUnknown: true,
};

export function validateSchema(o: any, shcema: any) {
  const {error, value} = shcema.validate(o, options);

  if (error) {
    throw new Error(`Validation failed with error: ${error}`);
  }
}
