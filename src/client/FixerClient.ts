import {CurrencyCode} from '../model/CurrencyCode';
import {config} from '../../config';
import axios from 'axios';
import {MoneyRateInterface} from '../utils/MoneyRateInterface';

export class FixerClient implements MoneyRateInterface {
  /**
   * Returns EUR rate to currency
   * @param currencyCode
   * @returns
   */
  async getRateToday(currencyCode: CurrencyCode): Promise<number> {
    const config2 = {
      headers: {
        apikey: config.flixerApiKey,
      },
    };
    const res = await axios.get(
      'https://api.apilayer.com/fixer/latest?base=EUR&symbols=' + currencyCode,
      config2
    );
    // let res: any = {};
    // res.data = {
    //   success: true,
    //   timestamp: 1677615963,
    //   base: 'EUR',
    //   date: '2023-02-28',
    //   rates: {GBP: 0.878165},
    // };
    const rate = res.data.rates[currencyCode];

    return rate;
  }
}
