import {Order} from '../model/Order';
import {OrdersRepository} from '../repository/OrdersRepository';
export class OrdersService {
  async postAnOrder(order: Order): Promise<void> {
    const ordersRepository = new OrdersRepository();
    await ordersRepository.postAnOrder(order);
  }
  findAllOrders(): Promise<Order[]> {
    const ordersRepository = new OrdersRepository();
    const orders = ordersRepository.findAll();
    return orders;
  }
}
