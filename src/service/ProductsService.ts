import {Product} from '../model/Product';
import {ProductsRepository} from '../repository/ProductsRepository';
import {CurrencyCode} from '../model/CurrencyCode';
import {PriceTransformer} from '../utils/PriceTransformer';

export class ProductsService{
  async getProduct(
    productId: string,
    currencyCode?: CurrencyCode
  ): Promise<Product> {
    const productsRepository = new ProductsRepository();
    let product = await productsRepository.findByProductId(productId);
    return product;
  }

  async getProductsByCategory(
    categoryId: string,
    currencyCode?: CurrencyCode
  ): Promise<Product[]> {
    const productsRepository = new ProductsRepository();
    let products = await productsRepository.findByCategoryId(categoryId);
    if (currencyCode && currencyCode != CurrencyCode.EUR) {
      const priceTransformer = new PriceTransformer();
      products = await priceTransformer.toCurrency(products, currencyCode);
    }
    return products;
  }
}
