import {Category} from '../model/Category';
import {CategoriesRepository} from '../repository/CategoriesRepository';

export class CategoriesService {
  async getAllCategories(): Promise<Category[]> {
    const categoriesRepository = new CategoriesRepository();
    return await categoriesRepository.findAll();
  }
}
