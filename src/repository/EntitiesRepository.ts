import {EntitiesRepositoryInterface} from './EntitiesRepositoryInterface';
import {config} from '../../config';

// const {MongoClient} = require('mongodb');
import * as mongoDB from 'mongodb';

// Connection URL
const url = config.mongoDbUrl;
const client = new mongoDB.MongoClient(url);
const databaseName = config.groceriesDatabaseName;

export abstract class EntitiesRepository<T>
  implements EntitiesRepositoryInterface<T>
{
  db: mongoDB.Db;
  collection: mongoDB.Collection;

  constructor(collectionsName: string) {
    this.db = client.db(databaseName);
    this.collection = this.db.collection(collectionsName);
  }
  async save(entity: T) {
    await this.collection.insertOne(entity as any);
  }
  async findAll(): Promise<T[]> {
    const insertResult = await this.collection.find();
    const res = insertResult.toArray() as unknown as T[];
    return res;
  }
  async findById(key: object): Promise<T[]> {
    const result = await this.collection.find(key);
    const res = result.toArray() as unknown as T[];
    return res;
  }

  async updateOne(key: object, entity: T) {
    const command = {$set: entity};
    const result = await this.collection.updateOne(key, command as any);
  }

  update(entity: T): T {
    throw new Error('Method not implemented.');
  }
}
