import {Order} from '../model/Order';
import {EntitiesRepository} from './EntitiesRepository';
import {config} from '../../config';
import {Product} from '../model/Product';
import {ProductsRepository} from './ProductsRepository';
const {MongoClient} = require('mongodb');
const url = config.mongoDbUrl;

const COLLECTIONS_NAME = config.ordersCollectionName;
export class OrdersRepository extends EntitiesRepository<Order> {
  constructor() {
    super(COLLECTIONS_NAME);
  }

  async postAnOrder(order: Order) {
    const client = new MongoClient(url);
    await client.connect();
    const session = client.startSession();
    const transactionOptions = {
      readPreference: 'primary',
      readConcern: {level: 'local'},
      writeConcern: {w: 'majority'},
    };
    try {
      await session.withTransaction(async () => {
        const coll1 = client
          .db(config.groceriesDatabaseName)
          .collection(config.productsCollectionName);
        const coll2 = client
          .db(config.groceriesDatabaseName)
          .collection(config.ordersCollectionName);
        const productsRepository = new ProductsRepository();
        for (const item of order.orderItems) {
          const product = await productsRepository.findByProductId(
            item.productId
          );
          if (!product) {
            throw new Error(`Product with id ${item.productId} does not exist`);
          }
          if (product.quantity >= item.quantity) {
            product.quantity -= item.quantity;
            await productsRepository.replace(product);
          } else {
            throw new Error('not enough inventory');
          }
        }
        await this.save(order);
      }, transactionOptions);
    } catch (err: any) {
      console.log(err);
      throw new Error(err.message);
    } finally {
      await session.endSession();
      await client.close();
    }
  }
}
