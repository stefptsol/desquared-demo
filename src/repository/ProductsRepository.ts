import {Product} from '../model/Product';
import {EntitiesRepository} from './EntitiesRepository';
import {config} from '../../config';

const COLLECTIONS_NAME = config.productsCollectionName;
export class ProductsRepository extends EntitiesRepository<Product> {
  constructor() {
    super(COLLECTIONS_NAME);
  }

  async findByProductId(productId: string): Promise<Product> {
    const res = await super.findById({_id: productId});
    return res[0];
  }

  async findByCategoryId(categoryId: string): Promise<Product[]> {
    return await super.findById({categoryId: categoryId});
  }

  async replace(product: Product) {
    await super.updateOne({_id: product._id}, product);
  }
}
