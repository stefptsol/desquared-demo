import {Category} from '../model/Category';
import {EntitiesRepository} from './EntitiesRepository';
import {config} from '../../config';

const COLLECTIONS_NAME = config.categoriesCollectionName;
export class CategoriesRepository extends EntitiesRepository<Category> {
  constructor() {
    super(COLLECTIONS_NAME);
  }

  async findByCategoryId(categoryId: string): Promise<Category[]> {
    return await super.findById({_id: categoryId});
  }
}
