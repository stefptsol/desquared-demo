export interface EntitiesRepositoryInterface<T> {
  save(entity: T): void;
  findById(key: object): Promise<T[]>;
  findAll(entityId: string): Promise<T[]>;
  update(entity: T): T;
  updateOne(key: object, entity: T): void;
}
