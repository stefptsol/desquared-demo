import {Router} from 'express';
import {OrdersController} from '../controller/OrdersController';

export const router = Router();
const ordersController = new OrdersController();
router.get('/orders', ordersController.viewAllOrders);
router.post('/orders', ordersController.createOrder);
