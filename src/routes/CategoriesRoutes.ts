import {Router} from 'express';
import {CategoriesController} from '../controller/CategoriesController';

export const categoriesRouter = Router();
const categoriesController = new CategoriesController();
categoriesRouter.get('/categories', categoriesController.findCategories);
