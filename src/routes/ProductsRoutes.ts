import {Router} from 'express';
import {ProductsController} from '../controller/ProductsController';

export const productsRouter = Router();
const productsController = new ProductsController();
productsRouter.get(
  '/categories/:categoryId/products',
  productsController.viewAllProducts
);
