export const config = {
  mongoDbUrl: 'mongodb://localhost:27017',
  flixerApiKey: 'add-your-flixer-key-here',
  groceriesDatabaseName: 'Groceries',
  productsCollectionName: 'Products',
  ordersCollectionName: 'Orders',
  categoriesCollectionName: 'Categories',
};
