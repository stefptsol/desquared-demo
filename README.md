## Prerequisites

for the server to run you will need to:

1. have Mongodb installed. Its url needs to be set on ./config.ts
2. add a fixer api key on ./config.ts

## To build

    npm i

Which installs npm dependencies

    npm run databaseInnit

Which saves to the database the required entities for the demo

## To start the server

      npm start

## Interface:

```
GET categories/
Description: returns all categories
```

```
GET categories/{categoryId}/products?currency=EUR
Description: returns the products of a category. Currency is an optional parameter which accepts a currency code.

```

```
Get orders/
Description: returns all orders

```

```
POST orders/
Description: creates an order
Body:
    {
        orderItems:[{
            productId: string;
            quantity: number;
        }]
    }

```

## Notes

1. The web page is on the source directory and it is called webpage.html
2. The vulnerability is on the dev dependencies on gts.
3. Order is saved with a database transaction.
4. App was developed in node 18.

## Corners cut

1. Money transactions are not done with a library such as big.js
2. Currency rate could be cashed
3. No security measures e.g. database connection, authentication and authorization
4. Error handling is limited
5. Unit testing is limited but it demonstrates mocha, chai and sinon usage
