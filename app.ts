import express, {Express, Request, Response} from 'express';
import {router} from './src/routes/OrderRoutes';
import {categoriesRouter} from './src/routes/CategoriesRoutes';
import {productsRouter} from './src/routes/ProductsRoutes';
const cors = require('cors');

const app: Express = express();
const port = 8000;
export const routes = express.Router();

app.use(express.json());
app.use(cors());
app.use(router);
app.use(categoriesRouter);
app.use(productsRouter);

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});
