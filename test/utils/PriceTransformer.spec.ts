import 'mocha';
import {PriceTransformer} from '../../src/utils/PriceTransformer';
import * as sinon from 'sinon';
import {Product} from '../../src/model/Product';
import {Price} from '../../src/model/Price';
import {CurrencyCode} from '../../src/model/CurrencyCode';
import {FixerClient} from '../../src/client/FixerClient';
import {expect} from 'chai';

describe('ProductRepository handler tests', () => {
  beforeEach(() => {
    sinon.stub(FixerClient.prototype, 'getRateToday').callsFake(async () => 2);
  });
  afterEach(() => {
    sinon.restore();
  });

  it.only('Should transform a price from EUR to GBP', async () => {
    const priceTransformer = new PriceTransformer();
    const price: Price = {
      value: 123,
      currency: CurrencyCode.EUR,
    };
    const product: Product = {
      _id: '444',
      categoryId: '123',
      price: price,
      description: 'asd',
      quantity: 2,
    };
    const price2: Price = {
      value: 321,
      currency: CurrencyCode.EUR,
    };
    const product2: Product = {
      _id: '444',
      categoryId: '123',
      price: price2,
      description: 'asd',
      quantity: 2,
    };
    const products = [];
    products.push(product);
    products.push(product2);

    const res = await priceTransformer.toCurrency(products, CurrencyCode.GBP);

    expect(res).to.not.be.undefined;
    expect(res[0].price.currency).to.equal('GBP');
    expect(res[0].price.value).to.equal(246);
    expect(res[1].price.currency).to.equal('GBP');
    expect(res[1].price.value).to.equal(642);
  });
});
