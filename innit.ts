import {Category} from './src/model/Category';
import {CurrencyCode} from './src/model/CurrencyCode';
import {Price} from './src/model/Price';
import {Product} from './src/model/Product';
import {CategoriesRepository} from './src/repository/CategoriesRepository';
import {ProductsRepository} from './src/repository/ProductsRepository';

async function innit(): Promise<void> {
  const categoriesRepository = new CategoriesRepository();
  const appetizers: Category = {
    _id: '555',
    name: 'Appetizers',
    description: 'initial dishes',
  };

  const drinks: Category = {
    _id: '444',
    name: 'Drinks',
    description: 'liquids',
  };

  const mainDishes: Category = {
    _id: '333',
    name: 'Main Dishes',
    description: 'basic menu',
  };

  await categoriesRepository.save(appetizers);
  await categoriesRepository.save(drinks);
  await categoriesRepository.save(mainDishes);

  const price: Price = {
    value: 2,
    currency: CurrencyCode.EUR,
  };

  const productsRepository = new ProductsRepository();
  const salad: Product = {
    _id: '123',
    categoryId: '555',
    price: price,
    description: 'salad',
    quantity: 5,
  };
  const redSalad: Product = {
    _id: '1',
    categoryId: '555',
    price: price,
    description: 'red salad',
    quantity: 5,
  };
  await productsRepository.save(salad);
  await productsRepository.save(redSalad);

  const amstel: Product = {
    _id: '556',
    categoryId: '444',
    price: price,
    description: 'amstel',
    quantity: 5,
  };
  const fanta: Product = {
    _id: '557',
    categoryId: '444',
    price: price,
    description: 'fanta',
    quantity: 5,
  };
  await productsRepository.save(amstel);
  await productsRepository.save(fanta);

  const rice: Product = {
    _id: '667',
    categoryId: '333',
    price: price,
    description: 'rice',
    quantity: 5,
  };
  const spaghetti: Product = {
    _id: '668',
    categoryId: '333',
    price: price,
    description: 'spaghetti',
    quantity: 5,
  };
  await productsRepository.save(rice);
  await productsRepository.save(spaghetti);
}

innit().then(() => process.exit());
